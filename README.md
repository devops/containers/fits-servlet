# FITS Servlet container image

Container image builder for https://github.com/harvard-lts/FITSservlet. Harvard does also
have a container project at https://github.com/harvard-lts/FITSServlet_Container, but
the build seemed out of date.

## Using the image

    $ docker pull gitlab-registry.oit.duke.edu/devops/containers/fits-servlet:latest

    $ docker run --rm -p 8080:8080 -v /mydata:/data gitlab-registry.oit.duke.edu/devops/containers/fits-servlet:latest

    $ curl http://localhost:8080/fits/version
