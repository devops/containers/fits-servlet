FROM debian:buster as fits

ARG FITS_VERSION
ARG FITS_HOME=/usr/src/fits

RUN apt-get -y update \
	&& apt-get -y install \
	unzip \
	wget

WORKDIR /tmp
RUN wget -q https://automation.lib.duke.edu/fits/fits-${FITS_VERSION}.zip \
	&& wget -q https://automation.lib.duke.edu/fits/fits-${FITS_VERSION}.zip.sha512 \
	&& sha512sum --status -c fits-${FITS_VERSION}.zip.sha512 \
	&& unzip -q fits-${FITS_VERSION}.zip -d ${FITS_HOME}

FROM tomcat:9-jdk8
#
# See https://hub.docker.com/_/tomcat.
#
# CATALINA_BASE:   /usr/local/tomcat
# CATALINA_HOME:   /usr/local/tomcat
# CATALINA_TMPDIR: /usr/local/tomcat/temp
# JRE_HOME:        /usr
# CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar
#
ARG FITS_HOME=/usr/src/fits
ARG FITS_SERVLET_VERSION
ARG FITS_SERVLET_PATH=fits

#
# We need to set the timezone because FITS has stupid date/times and is not
# timezone-aware.
# Our code assumes the FITS XML timestamp attribute is a local time since
# that's what it would normally be on a workstation or server with timezone
# configured correctly.
#
ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY --from=fits ${FITS_HOME} ${FITS_HOME}
RUN apt-get -y update \
	&& apt-get -y install \
	file \
	wget \
	&& apt-get -y clean

WORKDIR /tmp
RUN wget -q https://automation.lib.duke.edu/fits/fits-${FITS_SERVLET_VERSION}.war \
	&& wget -q https://automation.lib.duke.edu/fits/fits-${FITS_SERVLET_VERSION}.war.sha512 \
	&& sha512sum --status -c fits-${FITS_SERVLET_VERSION}.war.sha512 \
	&& mv fits-${FITS_SERVLET_VERSION}.war ${CATALINA_HOME}/webapps/${FITS_SERVLET_PATH}.war

WORKDIR $CATALINA_HOME
RUN echo "fits.home=${FITS_HOME}" >> conf/catalina.properties \
	&& sed -i -e 's|shared\.loader=||' conf/catalina.properties \
	&& echo 'shared.loader=${fits.home}/lib/*.jar' >> conf/catalina.properties

RUN mkdir -p -m 0755 /data
VOLUME /data
